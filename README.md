# Python Flask App Demo for GitOps

## CI Scripts

Config your `Settings -> CI/CD -> Variables` including `CI_REGISTRY`, `CI_REGISTRY_IMAGE`, `CI_REGISTRY_USER`, `CI_REGISTRY_PASSWORD`, `CI_USERNAME` and `CI_PASSWORD` and update your application code to trigger automation build and deploy.

+ CI_REGISTRY - Registry url
+ CI_REGISTRY_PASSWORD - Registry password
+ CI_REGISTRY_USER - Registry user name (kcllang)
+ CI_REGISTRY_IMAGE - App image name (flask_demo)
+ CI_USERNAME - Git user name
+ CI_PASSWORD - Git password

```yaml
stages:
- publish
- deploy

publish:
  stage: publish
  image:
    name: cnych/kaniko-executor:v0.22.0
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile ./Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  only:
    - main

deploy:
  stage: deploy
  image: cnych/kustomize:v1.0
  before_script:
    - git remote set-url origin https://gitlab.com/kcl-lang/flask-demo
    - git config --global user.email "gitlab@git.local"
    - git config --global user.name "GitLab CI/CD"
    # Install KCL
    - wget -q https://kcl-lang.io/script/install.sh -O - | /bin/bash
  script:
    - git checkout -B main
    - cd deployment
    # Image auto update
    - /usr/local/kclvm/bin/kcl -d -O config.containers.flask_demo.image="$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - git commit -am '[skip ci] image update to $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA'
    - git push origin main
  only:
    - main
```
